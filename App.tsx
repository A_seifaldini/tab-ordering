import React from "react";
import { StyleSheet, View, StatusBar } from "react-native";
import Animated from "react-native-reanimated";
import { tabs, TAB_SIZE, TAB_COLUMNS } from "./Tab";
import SortableTab from "./SortableTab";

const { Value } = Animated;

export default function App() {
	const offsets = tabs.map((tab, index) => ({
		x: new Value(index % TAB_COLUMNS === 0 ? 0 : TAB_SIZE),
		y: new Value(Math.floor(index / TAB_COLUMNS) * TAB_SIZE)
	}));
	return (
		<View style={styles.container}>
			{tabs.map((tab, index) => (
				<SortableTab key={tab.id} {...{ tab, index, offsets }} />
			))}
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#1c1d1e",
		marginTop: StatusBar.currentHeight
	}
});
